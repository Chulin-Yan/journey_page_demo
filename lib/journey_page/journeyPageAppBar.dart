import 'package:flutter/material.dart';
import 'package:journey_page/globalValues.dart';
import 'package:material_segmented_control/material_segmented_control.dart';

class JourneyPageAppBar {
  final _totalHeight;
  final _totalWidth;
  final Function _function;

  static int sharedValue = 0;
  JourneyPageAppBar(
    this._totalWidth,
    this._totalHeight,
    this._function,
  );

  Widget getAppBar() {
    // labels of journey map and activity 
    final Map<int, Widget> _label = <int, Widget>{
      0: Container(
        width: 0.4 * _totalWidth,
        alignment: Alignment.center,
        child: Text(
          'Journey Map',
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      1: Container(
        width: 0.4 * _totalWidth,
        alignment: Alignment.center,
        child: Text(
          'Activity',
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    };
    return AppBar(
      backgroundColor: Colors.white,
      title: Container(
        height: 0.03 * _totalHeight,
        alignment: Alignment.center,
        child: Text(
          "JOURNEY",
          style: TextStyle(
            color: Colors.black,
            fontSize: 20,
          ),
        ),
      ),
      bottom: PreferredSize(
        preferredSize: Size(
          _totalWidth,
          0.04 * _totalHeight,
        ),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(
              Radius.circular(60.0),
            ),
          ),
          child: SizedBox(
            width: 0.95 * _totalWidth,
            child: Container(
              padding: EdgeInsets.all(_totalHeight * 0.008),
              child: MaterialSegmentedControl(
                verticalOffset: 0.0,
                horizontalPadding: EdgeInsets.symmetric(
                  horizontal: 0.0,
                ),
                children: _label,
                selectionIndex: sharedValue,
                onSegmentChosen: (int value) {
                  sharedValue = value;
                  _function();
                },
                selectedColor: GlobalValues.PRIMARY_COLOR,
                unselectedColor: Colors.white,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
