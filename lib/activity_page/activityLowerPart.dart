import 'package:flutter/material.dart';
import 'package:journey_page/globalValues.dart';
import 'package:journey_page/activity_page/labelWidget.dart';
import 'package:journey_page/activity_page/repsOverviewControlUnit.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class ActivityLowerPart extends StatefulWidget {
  final double _totalWidth;
  final double _totalHeight;
  final List<double> _reps;

  ActivityLowerPart(
    this._totalWidth,
    this._totalHeight,
    this._reps,
  );
  @override
  _ActivityLowerPartState createState() => _ActivityLowerPartState();
}

class _ActivityLowerPartState extends State<ActivityLowerPart> {
  List<bool> _tapSensor = [
    false,
    false,
    false,
    false,
    false,
    false,
    false,
  ];

  void _changeSensorStatus(int index) {
    var temp = _tapSensor[index];
    for (int i = 0; i < _tapSensor.length; i++) {
      _tapSensor[i] = false;
    }
    _tapSensor[index] = !temp;
    // print('$index is changed');
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    double _controlUnitWidth = 0.115 * widget._totalWidth;
    double _controlUnitHeight = 0.35 * widget._totalHeight;
    // build check
    if (widget._reps.length != 7) {
      return Container(
        child: Center(
          child: Text(
              'Error. A list of 7 integers needs to be passed in. Only ${widget._reps.length} integers proveded.'),
        ),
      );
    }

    return Container(
      child: Column(
        children: [
          // graph title and icon
          Container(
            padding: EdgeInsets.fromLTRB(
              0.0,
              0.02 * widget._totalHeight,
              0.0,
              0.02 * widget._totalHeight,
            ),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(15.0),
                topRight: Radius.circular(15.0),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: 0.015 * widget._totalWidth,
                    vertical: 0.0,
                  ),
                  child: Text(
                    'Reps Overview',
                    style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: 0.015 * widget._totalWidth,
                    vertical: 0.0,
                  ),
                  child: Image.asset('lib/asset/Icon/edit-curves.png'),
                ),
              ],
            ),
          ),
          // graph
          Container(
            color: Colors.white,
            child: Stack(
              children: [
                SfCartesianChart(
                  plotAreaBorderColor: Colors.transparent,
                  plotAreaBorderWidth: 0.0,
                  primaryXAxis: CategoryAxis(
                    labelPosition: ChartDataLabelPosition.inside,
                    labelStyle: TextStyle(
                      fontSize: 12,
                      color: GlobalValues.PRIMARY_COLOR,
                    ),
                    axisLine: AxisLine(
                      color: Colors.transparent,
                    ),
                    majorGridLines: MajorGridLines(
                      width: 0.0,
                      color: Colors.transparent,
                    ),
                    majorTickLines: MajorTickLines(
                      color: Colors.transparent,
                    ),
                    rangePadding: ChartRangePadding.additional,
                    labelPlacement: LabelPlacement.onTicks,
                  ),
                  primaryYAxis: NumericAxis(
                    labelPosition: ChartDataLabelPosition.inside,
                    edgeLabelPlacement: EdgeLabelPlacement.hide,
                    rangePadding: ChartRangePadding.additional,
                    axisLine: AxisLine(
                      color: Colors.transparent,
                    ),
                    majorGridLines: MajorGridLines(
                      width: 0.3,
                      color: Colors.blue[50],
                    ),
                    majorTickLines: MajorTickLines(
                      color: Colors.transparent,
                    ),
                  ),
                  series: <CartesianSeries<DotPosition, String>>[
                    SplineAreaSeries<DotPosition, String>(
                      borderColor: Colors.red,
                      splineType: SplineType.monotonic,
                      dataLabelSettings: DataLabelSettings(
                          color: Colors.amber,
                          isVisible: true,
                          showZeroValue: true,
                          labelPosition: ChartDataLabelPosition.inside,
                          labelAlignment: ChartDataLabelAlignment.middle,
                          builder: (dynamic data, dynamic point, dynamic series,
                              int pointIndex, int seriesIndex) {
                            return data.isVisible == false
                                ? Container()
                                : Container(
                                    height: 0.08 * widget._totalHeight,
                                    width: 0.15 * widget._totalWidth,
                                    child: LabelWidget(
                                      data.y.toInt(),
                                      widget._totalWidth,
                                      widget._totalHeight,
                                    ),
                                  );
                          }),
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          GlobalValues.PRIMARY_COLOR,
                          GlobalValues.SECONDARY_COLOR,
                        ],
                      ),
                      opacity: 0.24,
                      dataSource: <DotPosition>[
                        DotPosition(
                          'Mon',
                          widget._reps[0],
                          _tapSensor[0],
                        ),
                        DotPosition(
                          'Tue',
                          widget._reps[1],
                          _tapSensor[1],
                        ),
                        DotPosition(
                          'Wed',
                          widget._reps[2],
                          _tapSensor[2],
                        ),
                        DotPosition(
                          'Thu',
                          widget._reps[3],
                          _tapSensor[3],
                        ),
                        DotPosition(
                          'Fri',
                          widget._reps[4],
                          _tapSensor[4],
                        ),
                        DotPosition(
                          'Sat',
                          widget._reps[5],
                          _tapSensor[5],
                        ),
                        DotPosition(
                          'Sun',
                          widget._reps[6],
                          _tapSensor[6],
                        ),
                      ],
                      xValueMapper: (DotPosition dot, _) {
                        return dot.x;
                      },
                      yValueMapper: (DotPosition dot, _) {
                        return dot.y;
                      },
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      width: 0.03 * widget._totalWidth,
                    ),
                    RepsOverviewControlUnit(0.5 * _controlUnitWidth,
                        _controlUnitHeight, _changeSensorStatus, 0),
                    RepsOverviewControlUnit(_controlUnitWidth,
                        _controlUnitHeight, _changeSensorStatus, 1),
                    RepsOverviewControlUnit(_controlUnitWidth,
                        _controlUnitHeight, _changeSensorStatus, 2),
                    RepsOverviewControlUnit(_controlUnitWidth,
                        _controlUnitHeight, _changeSensorStatus, 3),
                    RepsOverviewControlUnit(_controlUnitWidth,
                        _controlUnitHeight, _changeSensorStatus, 4),
                    RepsOverviewControlUnit(_controlUnitWidth,
                        _controlUnitHeight, _changeSensorStatus, 5),
                    RepsOverviewControlUnit(0.8 * _controlUnitWidth,
                        _controlUnitHeight, _changeSensorStatus, 6),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class DotPosition {
  final String x;
  final double y;
  bool isVisible;

  DotPosition(
    this.x,
    this.y,
    this.isVisible,
  );
}
