import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:journey_page/activity_page/activityLowerPart.dart';
import 'package:journey_page/activity_page/activityUpperPart.dart';
import 'package:journey_page/userInfo.dart';

// ignore: must_be_immutable
class Activity extends StatelessWidget {
  final List<Dot> _totalDots;
  DateTime _currentDate;
  List<int> _toUpperActivity = [];
  Map<DateTime, int> dateRepsMap = {};
  Activity(
    this._totalDots,
    this._currentDate,
  );

  Map<DateTime, int> getDateRepsMap() {
    Map<DateTime, int> dateRepsMap = {};
    for (int i = 0; i < _totalDots.length; i++) {
      if (_totalDots[i].day != null) {
        dateRepsMap.addAll(
          {
            _totalDots[i].day: _totalDots[i].finishedReps == null
                ? 0
                : _totalDots[i].finishedReps
          },
        );
      }
    }
    return dateRepsMap;
  }

  List<int> getRepsList(DateTime start, DateTime end) {
    List<int> result = [];
    for (DateTime temp = start;
        temp.isBefore(
      DateTime(end.year, end.month, end.day + 1),
    );
        temp = new DateTime(temp.year, temp.month, temp.day + 1),) {
      if (dateRepsMap.containsKey(temp)) {
        result.add(dateRepsMap[temp]);
      } else {
        result.add(0);
      }
    }
    return result;
  }

  List<double> getRepsListInDecimal(List<int> originalList) {
    List<double> result = [];
    for (int i = 0; i < originalList.length; i++) {
      result.add(originalList[i] + 0.00);
    }
    return result;
  }

  @override
  Widget build(BuildContext context) {
    final double _totalWidth = MediaQuery.of(context).size.width;
    final double _totalHeight = MediaQuery.of(context).size.height;
    if (_currentDate == null) {
      _currentDate = DateTime(
        2020,
        4,
        9,
      );
    }
    DateTime _startOfWeek = new DateTime(
      _currentDate.year,
      _currentDate.month,
      _currentDate.day - _currentDate.weekday + 1,
    );
    DateTime _endOfWeek = new DateTime(
      _currentDate.year,
      _currentDate.month,
      _currentDate.day + 7 - _currentDate.weekday,
    );
    DateTime _startOfMonth = new DateTime(
      _currentDate.year,
      _currentDate.month,
      1,
    );
    DateTime _endOfMonth = new DateTime(
      _currentDate.year,
      _currentDate.month + 1,
      0,
    );
    while (dateRepsMap.length == 0) {
      dateRepsMap = getDateRepsMap();
    }
    var _dailyReps = getRepsList(
      _currentDate,
      _currentDate,
    )[0];
    var _weeklyReps = 0;
    getRepsList(_startOfWeek, _endOfWeek).forEach((element) {
      _weeklyReps += element;
    });
    var _monthlyReps = 0;
    getRepsList(_startOfMonth, _endOfMonth).forEach((element) {
      _monthlyReps += element;
    });
    _toUpperActivity = [_dailyReps, _weeklyReps, _monthlyReps];
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            ActivityUpperPart(
              _totalWidth,
              _totalHeight,
              _toUpperActivity,
            ),
            Container(
              decoration: BoxDecoration(
                color: Colors.blue,
              ),
              child: ActivityLowerPart(
                _totalWidth,
                _totalHeight,
                getRepsListInDecimal(
                  getRepsList(
                    _startOfWeek,
                    _endOfWeek,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 0.05 * _totalHeight,
            ),
          ],
        ),
      ),
    );
  }
}
